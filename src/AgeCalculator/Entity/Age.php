<?php
/**
 * Created by PhpStorm.
 * User: agaizauskas
 * Date: 14/03/2017
 * Time: 15:04
 */

namespace AgeCalculator\Entity;


/**
 * Class Age
 * @package AgeCalculator\Entity
 */
class Age implements \JsonSerializable
{
    /**
     * @var \DateTime
     */
    protected $birthDate;

    /**
     * @var \DateInterval
     */
    protected $age;

    /**
     * @var int
     */
    protected $years = 0;

    /**
     * @var int
     */
    protected $months = 0;

    /**
     * @var int
     */
    protected $days = 0;

    /**
     * @var int
     */
    protected $hours = 0;

    /**
     * @var int
     */
    protected $minutes = 0;

    /**
     * @var int
     */
    protected $seconds = 0;

    /**
     * @var int
     */
    protected $dtd = 0;

    /**
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param \DateTime $birthDate
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return \DateInterval
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param \DateInterval $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return int
     */
    public function getYears()
    {
        return $this->years;
    }

    /**
     * @param int $years
     */
    public function setYears($years)
    {
        $this->years = $years;
    }

    /**
     * @return int
     */
    public function getMonths()
    {
        return $this->months;
    }

    /**
     * @param int $months
     */
    public function setMonths($months)
    {
        $this->months = $months;
    }

    /**
     * @return int
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * @param int $days
     */
    public function setDays($days)
    {
        $this->days = $days;
    }

    /**
     * @return int
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * @param int $hours
     */
    public function setHours($hours)
    {
        $this->hours = $hours;
    }

    /**
     * @return int
     */
    public function getMinutes()
    {
        return $this->minutes;
    }

    /**
     * @param int $minutes
     */
    public function setMinutes($minutes)
    {
        $this->minutes = $minutes;
    }

    /**
     * @return int
     */
    public function getSeconds()
    {
        return $this->seconds;
    }

    /**
     * @param int $seconds
     */
    public function setSeconds($seconds)
    {
        $this->seconds = $seconds;
    }

    /**
     * @return int
     */
    public function getDtd()
    {
        return $this->dtd;
    }

    /**
     * @param int $dtd
     */
    public function setDtd($dtd)
    {
        $this->dtd = $dtd;
    }

    /**
     * Entity output for auto serialization
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'birthDate' => $this->getBirthDate()->format("d/m/Y H:i:s"),
            'age'       => $this->getAge()->format("%Y years, %m months, %d days, %h hours, %i minutes, %s seconds"),
            'totals'    => $this->getTotalsArray(),
            'divided'   => $this->getTotalsArray(),
        );
    }

    /**
     * Age divided values as array
     *
     * @return array
     */
    public function getDividedArray()
    {
        return array(
            'Y' => $this->getAge()->y,
            'm' => $this->getAge()->m,
            'd' => $this->getAge()->d,
            'h' => $this->getAge()->h,
            'i' => $this->getAge()->i,
            's' => $this->getAge()->s,
        );
    }

    /**
     * Age total values as array
     *
     * @return array
     */
    public function getTotalsArray()
    {
        return array(
            'Y' => $this->getYears(),
            'm' => $this->getMonths(),
            'd' => $this->getMonths(),
            'h' => $this->getHours(),
            'i' => $this->getMinutes(),
            's' => $this->getSeconds(),
        );
    }
}