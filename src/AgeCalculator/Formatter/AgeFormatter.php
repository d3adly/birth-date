<?php
/**
 * Created by PhpStorm.
 * User: agaizauskas
 * Date: 14/03/2017
 * Time: 15:23
 */

namespace AgeCalculator\Formatter;


use AgeCalculator\Entity\Age;

/**
 * Class AgeFormatter
 * @package AgeCalculator\Formatter
 */
class AgeFormatter
{
    /**
     *
     */
    const FORMAT_TOTAL_YEARS = "years";

    /**
     *
     */
    const FORMAT_TOTAL_MONTHS = "months";

    /**
     *
     */
    const FORMAT_TOTAL_DAYS = "days";

    /**
     *
     */
    const FORMAT_TOTAL_HOURS = "hours";

    /**
     *
     */
    const FORMAT_TOTAL_MINUTES = "minutes";

    /**
     *
     */
    const FORMAT_TOTAL_SECONDS = "seconds";

    /**
     *
     */
    const FORMAT_DIVIDED_PRECISE = "dividedPrecise";

    /**
     *
     */
    const FORMAT_DIVIDED_YDH = "dividedYDH";

    /**
     *
     */
    const FORMAT_DIVIDED_ARRAY = "dividedArray";

    /**
     * @var string
     */
    protected $format;

    /**
     * AgeFormatter constructor.
     *
     * @param string $format
     */
    public function __construct($format = self::FORMAT_DIVIDED_PRECISE)
    {
        $this->format = $format;
    }

    /**
     * Aply selected formatting for age display
     *
     * @param Age $age
     *
     * @return bool | string
     */
    public function format(Age $age)
    {
        $formatMethod = "get" . ucfirst($this->getFormat());
        if (method_exists($age, $formatMethod)) {
            return $age->{$formatMethod}($age);
        }
        if (method_exists($this, $formatMethod)) {
            return $this->{$formatMethod}($age);
        }

        return false;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

    /**
     * Returns available formats as array
     *
     * @return array
     */
    public function getAvailableFormats()
    {
        $reflection = new \ReflectionClass($this);

        return $reflection->getConstants();
    }

    /**
     * Returns age in full precise time string
     *
     * @param Age $age
     *
     * @return string
     */
    protected function getDividedPrecise(Age $age)
    {
        return $age->getAge()->format("%Y years, %m months, %d days, %h hours, %m minutes, %s seconds");
    }

    /**
     * Returns age in years, days for last year, hours for last day
     *
     * @param Age $age
     *
     * @return string
     */
    protected function getDividedYDH(Age $age)
    {
        $years = $age->getYears();
        $dtd = $age->getDtd();
        $hours = $age->getAge()->h;

        return "$years years, $dtd days, $hours hours";
    }
}